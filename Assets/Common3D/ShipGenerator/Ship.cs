using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Ship {
	public List<ShipBlock> blocks = new List<ShipBlock>();
	public Material material;

	public Mesh GenerateMesh() {
		List<CombineInstance> combine = new List<CombineInstance>();
		foreach (ShipBlock block in this.blocks) {
			CombineInstance ci = block.GetCombineInstance();
			if (ci.mesh != null) {
				combine.Add(ci);
			}
		}
		Mesh mesh = new Mesh();
		mesh.CombineMeshes(combine.ToArray(), true, true);
		return mesh;
	}
}
