using System.Collections.Generic;

public class ReduceField : INoiseField {
  private struct Entry {
    public INoiseField field;
    public float scale;

    public Entry(INoiseField field, float scale) {
      this.field = field;
      this.scale = scale;
    }
  }

  public delegate float Reducer(float a, float v);
  private Reducer reducer;
  private List<Entry> fields;

  public ReduceField(Reducer reducer) {
    fields = new List<Entry>();
    this.reducer = reducer;
  }

  public void Add(INoiseField field, float scale = 1f) {
    fields.Add(new Entry(field, scale));
  }

  public float ValueAt(float x, float y, float z) {
    float result = 0;
    foreach (Entry entry in fields) {
      result = reducer(result, entry.field.ValueAt(x, y, z) * entry.scale);
    }
    return result;
  }
}

