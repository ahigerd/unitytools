using UnityEngine;

public class RandomField : INoiseField {
  private SpatialRandom rng;
  private float scale;

  public RandomField(float scale = 1f) {
    rng = new SpatialRandom();
    this.scale = scale;
  }

  public RandomField(uint seed, float scale = 1f) {
    rng = new SpatialRandom(seed);
    this.scale = scale;
  }

  public float ValueAt(float x, float y, float z) {
    return rng.FloatAt(new Vector4(x, y, z, 0)) * scale;
  }
}
