using UnityEngine;
using System;
using System.Collections.Generic;

public partial class Triangulator {
  private List<Vector2> points;
  private List<Triangle> triangles;
  private Rect bounds;
  private MultiMap<Tuple<int, int>, Triangle> nearbyTriangles = new MultiMap<Tuple<int, int>, Triangle>();

  public List<Triangle> Triangles {
    get { return this.triangles; }
  }

  public Rect BoundingBox {
    get { return this.bounds; }
  }

  public Triangulator(List<Vector2> points) {
    this.points = points;
    this.bounds = new Rect(points[0], Vector2.zero);
    foreach (Vector2 p in this.points) {
      if (p.x < this.bounds.xMin)
        this.bounds.xMin = p.x;
      else if (p.x > this.bounds.xMax)
        this.bounds.xMax = p.x;
      if (p.y < this.bounds.yMin)
        this.bounds.yMin = p.y;
      else if (p.y > this.bounds.yMax)
        this.bounds.yMax = p.y;
    }
    this.BowyerWatson();
  }

  private void BowyerWatson() {
    this.triangles = new List<Triangle>();
    this.triangles.Add(new Triangle(
      new Vector2(this.bounds.xMin - .1f, this.bounds.yMin - .1f),
      new Vector2(this.bounds.xMin - .1f, this.bounds.yMax + .1f),
      new Vector2(this.bounds.xMax + .1f, this.bounds.yMax + .1f)
    ));
    this.triangles.Add(new Triangle(
      new Vector2(this.bounds.xMin - .1f, this.bounds.yMin - .1f),
      new Vector2(this.bounds.xMax + .1f, this.bounds.yMin - .1f),
      new Vector2(this.bounds.xMax + .1f, this.bounds.yMax + .1f)
    ));
    List<Triangle> badTri = new List<Triangle>();
    foreach (Vector2 p in this.points) {
      badTri.Clear();
      foreach (Triangle tri in this.triangles) {
        if (tri.CircumcircleContains(p)) {
          badTri.Add(tri);
        }
      }
      foreach (Triangle tri in badTri) {
        this.triangles.Remove(tri);
        for (int i = 0; i < 3; i++) {
          Vector2 a = tri.vertices[i];
          Vector2 b = tri.vertices[(i+1)%3];
          bool shared = false;
          foreach (Triangle other in badTri) {
            if (tri != other && other.IsEdge(a, b)) {
              shared = true;
              break;
            }
          }
          if (!shared) {
            this.triangles.Add(new Triangle(a, b, p));
          }
        }
      }
    }

    badTri.Clear();
    foreach (Triangle tri in this.triangles) {
      for (int i = 0; i < 3; i++) {
        if ((tri.vertices[i].x < this.bounds.xMin - .01f || tri.vertices[i].x > this.bounds.xMax + .01f) &&
            (tri.vertices[i].y < this.bounds.yMin - .01f || tri.vertices[i].y > this.bounds.yMax + .01f)) {
          badTri.Add(tri);
          break;
        }
      }
    }
    foreach (Triangle tri in badTri) {
      this.triangles.Remove(tri);
    }

    this.triangles.Sort();
    for (int i = 0; i < this.triangles.Count; i++) {
      Triangle tri = this.triangles[i];
      int minX = Int32.MaxValue, minY = Int32.MaxValue;
      int maxX = Int32.MinValue, maxY = Int32.MinValue;
      for (int j = 0; j < 3; j++) {
        nearbyTriangles.Add(Tuple.Create((int)tri.vertices[j].x, (int)tri.vertices[j].y), tri);
        if (tri.vertices[j].x < minX)
          minX = (int)tri.vertices[j].x;
        else if (tri.vertices[j].x > maxX)
          maxX = (int)tri.vertices[j].x + 1;
        if (tri.vertices[j].y < minY)
          minY = (int)tri.vertices[j].y;
        else if (tri.vertices[j].y > maxY)
          maxY = (int)tri.vertices[j].y + 1;
      }
      for (int cy = minY; cy <= maxY; cy++) {
        for (int cx = minX; cx <= maxX; cx++) {
          if (!tri.Contains(new Vector2(cx, cy)) &&
              !tri.Contains(new Vector2(cx+1, cy)) &&
              !tri.Contains(new Vector2(cx, cy+1)) &&
              !tri.Contains(new Vector2(cx+1, cy+1))) {
            continue;
          }
          nearbyTriangles.Add(Tuple.Create(cx, cy), tri);
        }
      }
      if (tri.adjacencies.Count > 2) continue;
      for (int j = i + 1; j < this.triangles.Count; j++) {
        Triangle other = this.triangles[j];
        if (other.adjacencies.Count > 2 || other.adjacencies.Contains(tri)) continue;
        if (other.circumcenter.y - other.circumradius > tri.circumcenter.y + tri.circumradius) {
          // If the lowest point of the first circumcircle is higher than the highest point of the second circumcircle,
          // the triangles can't possibly be adjacent. This is far more permissive than it could be, but it's a cheap quick check.
          break;
        }
        if (tri.SharesEdge(other)) {
          tri.adjacencies.Add(other);
          other.adjacencies.Add(tri);
        }
        if (tri.adjacencies.Count > 2) break;
      }
    }
  }

  public Mesh GetMesh(bool includeWireframe = true) {
    MeshBuilder mb = new MeshBuilder();
    foreach (Triangle tri in this.triangles) {
      mb.Triangle(tri.vertices[0], tri.vertices[1], tri.vertices[2], Vector2.zero, Vector2.zero, Vector2.zero);
    }
    return mb.GetMesh(includeWireframe);
  }

  public Mesh GetDualMesh(bool voronoi = true) {
    MeshBuilder mb = new MeshBuilder();
    foreach (Triangle tri in this.triangles) {
      foreach (Triangle adj in tri.adjacencies) {
        if (voronoi) {
          mb.Triangle(tri.circumcenter, tri.circumcenter, adj.circumcenter, Vector2.zero, Vector2.zero, Vector2.zero);
        } else {
          mb.Triangle(tri.Barycenter, tri.Barycenter, adj.Barycenter, Vector2.zero, Vector2.zero, Vector2.zero);
        }
      }
    }
    return mb.GetMesh();
  }

  public Triangle TriangleAt(Vector2 point) {
    foreach (Triangle tri in nearbyTriangles[Tuple.Create((int)point.x, (int)point.y)]) {
      if (tri.Contains(point)) return tri;
    }
    return Triangle.Null;
  }
}
