using UnityEngine;

public class BellField : INoiseField {
  private Vector3 center;
  private float scale, exponent;

  public BellField(Vector3 center, float width, float scale = 1f) {
    this.center = center;
    this.exponent = 1f / (width * width * 2f);
    this.scale = scale;
  }

	public float ValueAt(float x, float y, float z) {
    float r = (new Vector3(x, y, z) - center).magnitude;
    return scale * Mathf.Exp(exponent * r * r);
  }
}
