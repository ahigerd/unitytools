using System.Collections.Generic;

public class SumField : INoiseField {
  private struct Entry {
    public INoiseField field;
    public float scale;

    public Entry(INoiseField field, float scale) {
      this.field = field;
      this.scale = scale;
    }
  }
  private List<Entry> fields;

  public SumField() {
    fields = new List<Entry>();
  }

  public void Add(INoiseField field, float scale = 1f) {
    fields.Add(new Entry(field, scale));
  }

  public float ValueAt(float x, float y, float z) {
    float result = 0;
    foreach (Entry entry in fields) {
      result += entry.field.ValueAt(x, y, z) * entry.scale;
    }
    return result;
  }
}
