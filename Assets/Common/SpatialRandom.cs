using UnityEngine;
using System.Collections.Generic;

public class SpatialRandom {
  private const float FINTMAX = (float)0xFFFFFFFFU;

	public static uint ValueAt(int x, int y, int z, uint seedToUse)
	{
		// based on Jenkins96 hash
		uint a = (uint)(0x9e3779b9 + x - z << 8);
		uint b = (uint)(0x9e3779b9 + y + z << 8);
		uint c = seedToUse;

		a -= b; a -= c; a ^= (c>>13);
		b -= c; b -= a; b ^= (a<<8);
		c -= a; c -= b; c ^= (b>>13);
		a -= b; a -= c; a ^= (c>>12);
		b -= c; b -= a; b ^= (a<<16);
		c -= a; c -= b; c ^= (b>>5);
		a -= b; a -= c; a ^= (c>>3);
		b -= c; b -= a; b ^= (a<<10);
		c -= a; c -= b; c ^= (b>>15);

		c += 12;

		a -= b; a -= c; a ^= (c>>13);
		b -= c; b -= a; b ^= (a<<8);
		c -= a; c -= b; c ^= (b>>13);
		a -= b; a -= c; a ^= (c>>12);
		b -= c; b -= a; b ^= (a<<16);
		c -= a; c -= b; c ^= (b>>5);
		a -= b; a -= c; a ^= (c>>3);
		b -= c; b -= a; b ^= (a<<10);
		c -= a; c -= b; c ^= (b>>15);

		c = c & 0xFFFFFFFF;
    return (c == 0xFFFFFFFF) ? 0 : c;
	}

  public uint Seed { get; private set; }

  public SpatialRandom() {
    this.Seed = (uint)(System.DateTime.Now.Ticks & 0xFFFFFFFF);
  }

  public SpatialRandom(uint seed) {
    this.Seed = seed;
  }

  public int ValueAt(Vector4 pos) {
    Vector4 p1 = new Vector4(Mathf.Floor(pos.x), Mathf.Floor(pos.y), Mathf.Floor(pos.z), Mathf.Floor(pos.w));
    Vector4 p2 = new Vector4(Mathf.Ceil(pos.x), Mathf.Ceil(pos.y), Mathf.Ceil(pos.z), Mathf.Ceil(pos.w));
    int v1 = this.ValueAt((int)p1.x, (int)p1.y, (int)p1.z, (int)p1.w);
    int v2 = this.ValueAt((int)p2.x, (int)p2.y, (int)p2.z, (int)p2.w);
    if (v1 == v2) {
      return v1;
    }
    float t = (pos - p1).magnitude / (p2 - p1).magnitude;
    return (int)Mathf.Round(Mathf.Lerp(v1, v2, t));
  }

  public int ValueAt(int x, int y = 0, int z = 0, int seedOffset = 0) {
    return (int)SpatialRandom.ValueAt(x, y, z, Seed ^ (uint)seedOffset);
  }

  public float FloatAt(Vector4 pos) {
    return (uint)ValueAt(pos) / FINTMAX;
  }

  public float FloatAt(int x, int y = 0, int z = 0, int seedOffset = 0) {
    return (uint)this.ValueAt(x, y, z, seedOffset) / FINTMAX;
  }

  public IEnumerator<float> RandomSequence() {
    return RandomSequence(Vector4.zero, Vector3.forward);
  }

  public IEnumerator<float> RandomSequence(Vector4 start) {
    return RandomSequence(start, Vector3.forward);
  }

  public IEnumerator<float> RandomSequence(Vector4 start, Vector4 step) {
    Vector4 pos = start;
    while (true) {
      yield return FloatAt(pos);
      pos += step;
    }
  }
}
