using System;
using System.Collections.Generic;

public class MultiMap<TKey, TValue> {
  private Dictionary<TKey, HashSet<TValue>> data;

  public MultiMap() {
    data = new Dictionary<TKey, HashSet<TValue>>();
  }

  public MultiMap(MultiMap<TKey, TValue> other) {
    data = new Dictionary<TKey, HashSet<TValue>>();
    foreach (var kv in other.data) {
      data.Add(kv.Key, new HashSet<TValue>(kv.Value));
    }
  }

  public ISet<TValue> this[TKey key] {
    get {
      HashSet<TValue> bucket;
      if (!data.TryGetValue(key, out bucket)) {
        bucket = new HashSet<TValue>();
        data[key] = bucket;
      }
      return bucket;
    }
  }

  public bool Add(TKey key, TValue value) {
    return this[key].Add(value);
  }

  // TODO: Remove(TKey, TValue), RemoveAll(TValue), Remove(TKey), Find(TValue)

  public bool ContainsKey(TKey key) {
    return data.ContainsKey(key) && data[key].Count > 0;
  }

  public Dictionary<TKey, HashSet<TValue>>.KeyCollection Keys {
    get { return data.Keys; }
  }

  public IEnumerable<TValue> Values {
    get {
      HashSet<TValue> visited = new HashSet<TValue>();
      foreach (var bucket in data.Values) {
        foreach (var value in bucket) {
          if (visited.Add(value))
            yield return value;
        }
      }
    }
  }
}
