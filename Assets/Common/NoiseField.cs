﻿using UnityEngine;

public class NoiseField : INoiseField {
	private static float Dot(int[] g, float x, float y, float z) {
		return g[0] * x + g[1] * y + g[2] * z;
	}

	private static readonly int[][] grad3 = {
		new int[] { 1, 1, 0},
		new int[] {-1, 1, 0},
		new int[] { 1,-1, 0},
		new int[] {-1,-1, 0},
		new int[] { 1, 0, 1},
		new int[] {-1, 0, 1},
		new int[] { 1, 0,-1},
		new int[] {-1, 0,-1},
		new int[] { 0, 1, 1},
		new int[] { 0,-1, 1},
		new int[] { 0, 1,-1},
		new int[] { 0,-1,-1}
	};

	public static float noise(float xin, float yin, float zin, uint seed, float scale = 1.0f) {
    // modify input coordinates to adjust noise feature scale
		xin /= scale;
		yin /= scale;
		zin /= scale;
		float n0, n1, n2, n3;

		// skew input space
		float s = (xin + yin + zin) / 3.0f;
		int i = (int)Mathf.Floor(xin + s);
		int j = (int)Mathf.Floor(yin + s);
		int k = (int)Mathf.Floor(zin + s);
		float t = (i + j + k) / 6.0f;

		// unskew
		float X0 = i - t;
		float Y0 = j - t;
		float Z0 = k - t;
		// x,y,z distance from cell origin
		float x0 = xin - X0;
		float y0 = yin - Y0;
		float z0 = zin - Z0;

		// offsets for two reference corners
		int i1 = 0, j1 = 0, k1 = 0;
		int i2 = 0, j2 = 0, k2 = 0;
		if(x0 > y0) {
			if(y0 > z0) {
				i1 = 1;
				i2 = 1;
				j2 = 1;
			} else if(x0 > z0) {
				i1 = 1;
				i2 = 1;
				k2 = 1;
			} else {
				k1 = 1;
				i2 = 1;
				k2 = 1;
			}
		} else {
			if(y0 < z0) {
				k1 = 1;
				j2 = 1;
				k2 = 1;
			} else if(x0 < z0) {
				j1 = 1;
				j2 = 1;
				k2 = 1;
			} else {
				j1 = 1;
				i2 = 1;
				j2 = 1;
			}
		}

		// other corner offsets unskewed
		float x1 = x0 - i1 + 1f/6f;
		float y1 = y0 - j1 + 1f/6f;
		float z1 = z0 - k1 + 1f/6f;
		float x2 = x0 - i2 + 1f/3f;
		float y2 = y0 - j2 + 1f/3f;
		float z2 = z0 - k2 + 1f/3f;
		float x3 = x0 - 1f + 1f/2f;
		float y3 = y0 - 1f + 1f/2f;
		float z3 = z0 - 1f + 1f/2f;

		// generate pseudorandom values for corners
		uint gi0 = SpatialRandom.ValueAt(i, j, k, seed) % 12;
		uint gi1 = SpatialRandom.ValueAt(i+i1, j+j1, k+k1, seed) % 12;
		uint gi2 = SpatialRandom.ValueAt(i+i2, j+j2, k+k2, seed) % 12;
		uint gi3 = SpatialRandom.ValueAt(i+1, j+1, k+1, seed) % 12;

		// calculate contributions from 3 corners
		float t0 = 0.5f - x0 * x0 - y0 * y0 - z0 * z0;
		if(t0 <= 0) {
			n0 = 0f;
		} else {
			t0 *= t0;
			n0 = t0 * t0 * Dot(grad3[gi0], x0, y0, z0);
		}

		float t1 = 0.5f - x1 * x1 - y1 * y1 - z1 * z1;
		if(t1 <= 0) {
			n1 = 0f;
		} else {
			t1 *= t1;
			n1 = t1 * t1 * Dot(grad3[gi1], x1, y1, z1);
		}

		float t2 = 0.5f - x2 * x2 - y2 * y2 - z2 * z2;
		if(t2 <= 0) {
			n2 = 0f;
		} else {
			t2 *= t2;
			n2 = t2 * t2 * Dot(grad3[gi2], x2, y2, z2);
		}

		float t3 = 0.5f - x3 * x3 - y3 * y3 - z3 * z3;
		if(t3 <= 0) {
			n3 = 0f;
		} else {
			t3 *= t3;
			n3 = t3 * t3 * Dot(grad3[gi3], x3, y3, z3);
		}

		return (n0 + n1 + n2 + n3) * 32f;
	}

	private uint seed;
	private int octaves;
	private float scale, depth;

	public NoiseField(uint seed, int octaves = 1, float scale = 32.0f, float depth = 7000.0f) {
		this.seed = seed;
		this.octaves = octaves;
		this.scale = scale;
		this.depth = depth;
	}

	public float ValueAt(float x, float y, float z) {
		float result = 0;
		float oScale = scale;
		float oDepth = depth;
		for(uint i = 0; i < octaves; i++) {
			result += noise(x, y, z, seed + i, oScale) * oDepth;
			oScale *= 2f;
			oDepth /= 2f;
		}
		return result;
	}
}
