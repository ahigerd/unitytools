using UnityEngine;
using System.Collections;

public interface INoiseField {
	float ValueAt(float x, float y, float z);
}
