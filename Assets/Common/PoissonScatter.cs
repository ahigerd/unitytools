using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoissonScatter {
  private class Grid {
    public float width, height;
    private int[,] cells;
    public List<Vector2> points;
    public float cellSize;

    public int Count {
      get { return points.Count; }
    }

    public Grid(float width, float height, float cellSize) {
      this.cellSize = cellSize;
      this.width = width;
      this.height = height;
      int gridW = (int)(width / cellSize + 1);
      int gridH = (int)(height / cellSize + 1);
      this.cells = new int[gridW, gridH];
      this.points = new List<Vector2>();
      for (int y = 0; y < gridH; y++) {
        for (int x = 0; x < gridW; x++) {
          this.cells[x, y] = -1;
        }
      }
    }

    public int Add(Vector2 p) {
      int index = this.points.Count;
      this.points.Add(p);
      cells[(int)(p.x / cellSize), (int)(p.y / cellSize)] = index;
      return index;
    }

    public int Add(float x, float y) {
      return Add(new Vector2(x, y));
    }

    public int GetIndex(float x, float y) {
      if (x < 0 || y < 0 || x >= width || y >= height) return -1;
      return cells[(int)(x / cellSize), (int)(y / cellSize)];
    }

    public Vector2 GetPoint(float x, float y) {
      return GetPoint(GetIndex(x, y));
    }

    public Vector2 GetPoint(int index) {
      if (index < 0 || index >= points.Count) return Vector2.zero;
      return points[index];
    }
  }

  private float cellSize;
  private Grid grid;
  private float spread;
  private SpatialRandom random;
  private IEnumerator<float> rng;

  private float randomValue {
    get {
      rng.MoveNext();
      return rng.Current;
    }
  }

  private int randInt(int max) {
    return (int)(randomValue * max);
  }

  private int randInt(float max) {
    return randInt((int)max);
  }

  public PoissonScatter(float width, float height, float spread, uint seed = 0) {
    random = new SpatialRandom(seed);
    rng = random.RandomSequence();

    cellSize = spread / Mathf.Sqrt(2);
    grid = new Grid(width, height, spread / Mathf.Sqrt(2));
    this.spread = spread;

    int index = grid.Add(randInt(width), randInt(height));
    List<int> working = new List<int>{ index };

    while (working.Count > 0) {
      int workIndex = working[randInt(working.Count)];
      Vector2 p = grid.GetPoint(workIndex);
      for (int i = 0; i < 30; i++) {
        float dist = (1 + randomValue) * spread;
        float angle = randomValue * Mathf.PI * 2;
        float x = p.x + Mathf.Cos(angle) * dist;
        float y = p.y + Mathf.Sin(angle) * dist;
        if (x < 0 || y < 0 || x > grid.width || y > grid.height) continue;
        Vector2 other = NearestPoint(new Vector2(x, y), true);
        if (other == Vector2.zero) {
          index = grid.Add(x, y);
          working.Add(index);
        } else {
          working.Remove(workIndex);
        }
      }
    }
  }

  public Vector2 NearestPoint(Vector2 p, bool approximate = false) {
    Vector2 nearestPoint = Vector2.zero;
    float nearestDist = spread;
    for (float tx = p.x - 2 * cellSize; tx < p.x + 2.1f * cellSize; tx += cellSize) {
      for (float ty = p.y - 2 * cellSize; ty < p.y + 2.1f * cellSize; ty += cellSize) {
        Vector2 other = grid.GetPoint(tx, ty);
        if (other == Vector2.zero) continue;
        float dist = (other - p).magnitude;
        if (dist < nearestDist) {
          if (approximate) return other;
          nearestDist = dist;
          nearestPoint = other;
        }
      }
    }
    return nearestPoint;
  }

  public int Count {
    get { return grid.points.Count; }
  }

  public Vector2 GetPoint(int index) {
    return grid.GetPoint(index);
  }

  public IEnumerable<Vector2> Points {
    get {
      for (int i = 0; i < grid.Count; i++) {
        yield return grid.GetPoint(i);
      }
    }
  }
}
