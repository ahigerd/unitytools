using UnityEngine;
using System.Collections.Generic;

public partial class Triangulator {
  public struct Triangle : System.IEquatable<Triangle>, System.IComparable<Triangle> {
    public static Triangle Null = new Triangle(null);

    public Vector2[] vertices;
    public Vector2 circumcenter;
    public float circumradius;
    public List<Triangle> adjacencies;

    public bool IsNull {
      get { return vertices == null; }
    }

    public Vector2 Barycenter {
      get {
        return new Vector2(
          (vertices[0].x + vertices[1].x + vertices[2].x) / 3,
          (vertices[0].y + vertices[1].y + vertices[2].y) / 3
        );
      }
    }

    private Triangle(Object isNull) {
      vertices = null;
      adjacencies = null;
      circumcenter = Vector2.zero;
      circumradius = 0;
    }

    public Triangle(Vector2 a, Vector2 b, Vector2 c) {
      adjacencies = new List<Triangle>();
      float ax2 = a.x * a.x, ay2 = a.y * a.y;
      float bx2 = b.x * b.x, by2 = b.y * b.y;
      float cx2 = c.x * c.x, cy2 = c.y * c.y;
      float al = ax2 + ay2, bl = bx2 + by2, cl = cx2 + cy2;
      float ybc = b.y - c.y, yca = c.y - a.y, yab = a.y - b.y;
      float xcb = c.x - b.x, xac = a.x - c.x, xba = b.x - a.x;
      float d = 2 * (a.x * ybc + b.x * yca + c.x * yab);
      float ux = (al * ybc + bl * yca + cl * yab) / d;
      float uy = (al * xcb + bl * xac + cl * xba) / d;

      float angle = Vector2.SignedAngle(b - a, c - a);
      if (angle < 0) {
        vertices = new Vector2[]{ a, b, c };
      } else {
        vertices = new Vector2[]{ a, c, b };
      }
      circumcenter = new Vector2(ux, uy);
      circumradius = (a - circumcenter).magnitude;
    }

    public override string ToString() {
      if (IsNull) return "<null>";
      return string.Format(
        "<({0:0.0},{1:0.0})-({2:0.0},{3:0.0})-({4:0.0},{5:0.0})>",
        vertices[0].x, vertices[0].y,
        vertices[1].x, vertices[1].y,
        vertices[2].x, vertices[2].y
      );
    }

    public bool CircumcircleContains(Vector2 p) {
      return (p - circumcenter).magnitude <= circumradius;
    }

    public bool IsEdge(Vector2 a, Vector2 b) {
      for (int i = 0; i < 3; i++) {
        if ((vertices[i] == a && vertices[(i + 1) % 3] == b) ||
            (vertices[i] == b && vertices[(i + 1) % 3] == a)) {
          return true;
        }
      }
      return false;
    }

    public bool SharesEdge(Triangle other, out Vector2 edgeStart, out Vector2 edgeEnd) {
      for (int i = 0; i < 3; i++) {
        Vector2 a = other.vertices[i];
        Vector2 b = other.vertices[(i + 1) % 3];
        if (IsEdge(a, b)) {
          edgeStart = a;
          edgeEnd = b;
          return true;
        }
      }
      edgeStart = Vector2.zero;
      edgeEnd = Vector2.zero;
      return false;
    }

    public bool SharesEdge(Triangle other) {
      Vector2 dummy1, dummy2;
      return SharesEdge(other, out dummy1, out dummy2);
    }

    public bool Equals(Triangle other) {
      return (other.vertices[0] == vertices[0] || other.vertices[1] == vertices[0] || other.vertices[2] == vertices[0]) &&
        (other.vertices[0] == vertices[1] || other.vertices[1] == vertices[1] || other.vertices[2] == vertices[1]) &&
        (other.vertices[0] == vertices[2] || other.vertices[1] == vertices[2] || other.vertices[2] == vertices[2]);
    }

    public override bool Equals(System.Object other) {
      if (other is Triangle) {
        return this.Equals((Triangle)other);
      }
      return false;
    }

    public override int GetHashCode() {
      return vertices[0].GetHashCode() ^
        (vertices[1].GetHashCode() << 2) &
        (vertices[2].GetHashCode() >> 2);
    }

    public static bool operator==(Triangle lhs, Triangle rhs) {
      return lhs.Equals(rhs);
    }

    public static bool operator!=(Triangle lhs, Triangle rhs) {
      return !lhs.Equals(rhs);
    }

    public int CompareTo(Triangle other) {
      float diff = circumcenter.y - other.circumcenter.y;
      if (diff == 0) {
        diff = circumcenter.x - other.circumcenter.x;
      }
      return (int)Mathf.Sign(diff);
    }

    public bool Contains(Vector2 point) {
      Vector2 v0 = vertices[2] - vertices[0];
      Vector2 v1 = vertices[1] - vertices[0];
      point -= vertices[0];
      float dot00 = Vector2.Dot(v0, v0);
      float dot01 = Vector2.Dot(v0, v1);
      float dot02 = Vector2.Dot(v0, point);
      float dot11 = Vector2.Dot(v1, v1);
      float dot12 = Vector2.Dot(v1, point);
      float d = 1 / (dot00 * dot11 - dot01 * dot01);
      float u = (dot11 * dot02 - dot01 * dot12) * d;
      float v = (dot00 * dot12 - dot01 * dot02) * d;
      return u >= 0 && v >= 0 && (u + v) <= 1;
    }
  }
}
