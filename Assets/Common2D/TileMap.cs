using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class TileMap : LevelAwareBehaviour {
  private static int MAP_WIDTH = 512;
  private static int MAP_HEIGHT = 512;
	private static float BACKGROUND_DEPTH = -0.1f;
	private static float TERRAIN_DEPTH = -0.3f;

	public List<EnemyCore> enemyPrefabs = new List<EnemyCore>();
	public List<EnemyCore> collectPrefabs = new List<EnemyCore>();
  private List<EnemyCore> enemies = new List<EnemyCore>();

	public Material material;
	public Texture2D backgroundTexture;
	private Material backgroundMaterial;
	private Mesh backgroundMesh;
  private Mesh mapMesh = new Mesh();

  private byte[,] tiles = new byte[MAP_WIDTH, MAP_HEIGHT];
	private static float tileSizeH = 1f / 8f;
	private static float tileSizeV = 1f / 12f;
	private static Vector2 TileToUV(int x, int y) {
		return new Vector2(x * tileSizeH, (11 - y) * tileSizeV);
	}
  // TODO: This will need updated on a per-application basis
	private static Vector2[] tileUV = {
		/* .0. 0*0 .0. */ TileToUV(4, 3),
		/* .0. 0*0 .1. */ TileToUV(3, 3),
		/* .0. 0*1 .0. */ TileToUV(0, 3),
		/* .0. 0*1 .1. */ TileToUV(0, 0),
		/* .0. 1*0 .0. */ TileToUV(2, 3),
		/* .0. 1*0 .1. */ TileToUV(2, 0),
		/* .0. 1*1 .0. */ TileToUV(1, 3),
		/* .0. 1*1 .1. */ TileToUV(1, 0),
		/* .1. 0*0 .0. */ TileToUV(3, 5),
		/* .1. 0*0 .1. */ TileToUV(3, 4),
		/* .1. 0*1 .0. */ TileToUV(0, 2),
		/* .1. 0*1 .1. */ TileToUV(0, 1),
		/* .1. 1*0 .0. */ TileToUV(2, 2),
		/* .1. 1*0 .1. */ TileToUV(2, 1),
		/* .1. 1*1 .0. */ TileToUV(1, 2),
		/* .1. 1*1 .1. */ TileToUV(1, 1),

		/* 0:0 :*: 0:0 */ TileToUV(1, 1),
		/* 0:0 :*: 0:1 */ TileToUV(1, 1),
		/* 0:0 :*: 1:0 */ TileToUV(1, 1),
		/* 0:0 :*: 1:1 */ TileToUV(4, 2),
		/* 0:1 :*: 0:0 */ TileToUV(1, 1),
		/* 0:1 :*: 0:1 */ TileToUV(5, 1),
		/* 0:1 :*: 1:0 */ TileToUV(1, 1),
		/* 0:1 :*: 1:1 */ TileToUV(5, 2),
		/* 1:0 :*: 0:0 */ TileToUV(1, 1),
		/* 1:0 :*: 0:1 */ TileToUV(1, 1),
		/* 1:0 :*: 1:0 */ TileToUV(3, 1),
		/* 1:0 :*: 1:1 */ TileToUV(3, 2),
		/* 1:1 :*: 0:0 */ TileToUV(4, 0),
		/* 1:1 :*: 0:1 */ TileToUV(5, 0),
		/* 1:1 :*: 1:0 */ TileToUV(3, 0),
		/* 1:1 :*: 1:1 */ TileToUV(1, 1),
	};

	public void GenerateTiles() {
    // TODO: Application-specific code
		PopulateMesh();
	}

	public void Awake()
	{
		backgroundMaterial = new Material(material);
		backgroundMaterial.mainTexture = backgroundTexture;
		backgroundMesh = new Mesh();
		backgroundMesh.Clear();
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
		vertices.Add(new Vector3(0, 0, 0));
		vertices.Add(new Vector3(MAP_WIDTH, 0, 0));
		vertices.Add(new Vector3(MAP_WIDTH, -MAP_HEIGHT, 0));
		vertices.Add(new Vector3(0, -MAP_HEIGHT, 0));
		uv.Add(new Vector2(0, 0));
		uv.Add(new Vector2(1, 0));
		uv.Add(new Vector2(1, 1));
		uv.Add(new Vector2(0, 1));
		triangles.Add(0);
		triangles.Add(1);
		triangles.Add(2);
		triangles.Add(2);
		triangles.Add(3);
		triangles.Add(0);
		backgroundMesh.vertices = vertices.ToArray();
		backgroundMesh.uv = uv.ToArray();
		backgroundMesh.triangles = triangles.ToArray();

    GenerateTiles();
		PopulateMesh();
	}

	public override void OnGameStart() {
		foreach (EnemyCore enemy in enemies) {
			Destroy(enemy.gameObject);
		}
		enemies.Clear();
	}

	public override void OnPlayerSpawn() {
	}

	public void NewLevel() {
    GenerateTiles();
    PopulateMesh();
	}

	public byte TileTypeAt(float x, float y)
	{
		if (x < 0 || y < 0 || x >= MAP_WIDTH || y >= MAP_HEIGHT) {
			return (byte)65;
		}

		return tiles[(int)x, (int)y];
	}

	public bool IsSolid(float x, float y)
	{
		byte tileType = TileTypeAt(x, y);
		return tileType > 64;
	}

	public void PopulateMesh()
	{
		mapMesh.Clear();
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
		int triangleBase = 0;
		for (int y = 0; y < MAP_HEIGHT; y++) {
			for (int x = 0; x < MAP_WIDTH; x++) {
        byte tile = TileTypeAt(x, y);
				if (tile == 0) continue;
				vertices.Add(new Vector3(x, -y, 0));
				vertices.Add(new Vector3(x + 1, -y, 0));
				vertices.Add(new Vector3(x + 1, -y - 1, 0));
				vertices.Add(new Vector3(x, -y - 1, 0));
				int bits = 0;
				if (IsSolid(x, y-1)) bits |= 8;
				if (IsSolid(x-1, y)) bits |= 4;
				if (IsSolid(x+1, y)) bits |= 2;
				if (IsSolid(x, y+1)) bits |= 1;
				if (bits == 15) {
					bits = 16;
					if (IsSolid(x-1, y-1)) bits |= 8;
					if (IsSolid(x+1, y-1)) bits |= 4;
					if (IsSolid(x-1, y+1)) bits |= 2;
					if (IsSolid(x+1, y+1)) bits |= 1;
				}
				Vector2 uvPos = tileUV[bits];
				uv.Add(new Vector2(uvPos.x,            uvPos.y + tileSizeV));
				uv.Add(new Vector2(uvPos.x + tileSizeH, uvPos.y + tileSizeV));
				uv.Add(new Vector2(uvPos.x + tileSizeH, uvPos.y));
				uv.Add(new Vector2(uvPos.x,            uvPos.y));
				triangles.Add(triangleBase);
				triangles.Add(triangleBase+1);
				triangles.Add(triangleBase+2);
				triangles.Add(triangleBase);
				triangles.Add(triangleBase+2);
				triangles.Add(triangleBase+3);
				triangleBase += 4;
			}
		}
		mapMesh.vertices = vertices.ToArray();
		mapMesh.uv = uv.ToArray();
		mapMesh.triangles = triangles.ToArray();
	}

	public void Update() {
    Graphics.DrawMesh(backgroundMesh, new Vector3(0, 0, TileMap.BACKGROUND_DEPTH), Quaternion.identity, backgroundMaterial, 0, null, 0, null, false, false, false);
    Graphics.DrawMesh(mapMesh, new Vector3(0, 0, TileMap.TERRAIN_DEPTH), Quaternion.identity, material, 0, null, 0, null, false, false, false);
	}
}
