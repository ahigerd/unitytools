using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CharacterCore : LevelAwareBehaviour
{
  public static float PIXEL = 1f / 16f;
  public static float GRAVITY = 20f;
  public float height = 1f;
  public float speed = 4f;
  public float jumpPower = 6f;
  public float layer = 0f;
  public Vector2 hitBoxSize = new Vector2(0.6f, 0.8f);
  public AudioClip deathSound = null;
  protected AudioSource audioSource;
  protected bool isGrounded;

  private TileMap _tileMap = null;
  public TileMap tileMap
  {
    get
    {
      if (_tileMap == null) {
        _tileMap = GameObject.Find("Map").GetComponent<TileMap>();
      }
      return _tileMap;
    }
  }

  public Vector2 origin
  {
    get
    {
      return WorldToMapPoint(transform.position);
    }
    set
    {
      transform.position = MapToWorldPoint(value, layer);
    }
  }

  public Vector2 midpoint
  {
    get
    {
      return origin - new Vector2(0, hitBoxSize.y / 2f);
    }
  }

  public Rect hitBox
  {
    get
    {
      return HitBoxAt(origin);
    }
  }

  public static Vector2 WorldToMapPoint(Vector3 point)
  {
    return new Vector2(point.x, -point.y);
  }

  public static Vector3 MapToWorldPoint(Vector2 point, float layer = 0f)
  {
    return new Vector3(point.x, -point.y, layer);
  }

  public Rect HitBoxAt(Vector2 pos)
  {
    return new Rect(pos.x - hitBoxSize.x / 2f, pos.y - hitBoxSize.y, hitBoxSize.x, hitBoxSize.y);
  }

  public virtual void Start()
  {
    audioSource = gameObject.AddComponent<AudioSource>();
    SpriteAnimationManager sam = gameObject.GetComponent<SpriteAnimationManager>() as SpriteAnimationManager;
    if (sam) {
      sam.SwitchAnimation("Idle", true);
    }
    Rigidbody2D rb = gameObject.AddComponent<Rigidbody2D>() as Rigidbody2D;
    rb.bodyType = RigidbodyType2D.Kinematic;
    rb.useFullKinematicContacts = true;
    BoxCollider2D c2d = gameObject.AddComponent<BoxCollider2D>() as BoxCollider2D;
    c2d.size = hitBoxSize;
    c2d.offset = new Vector2(0f, hitBoxSize.y / 2);
    isGrounded = false;
  }

  public bool IsGrounded()
  {
    return isGrounded;
  }

  public virtual Vector2 Move(Vector2 v)
  {
    Vector2 delta = v * Time.deltaTime;
    Vector2 target = origin + delta;

    Rect oldHB = hitBox;
    Rect newHB = HitBoxAt(target);

    // First sweep horizontal motion
    if (v.x > 0 && (tileMap.IsSolid(newHB.xMax + PIXEL, oldHB.yMax - PIXEL) || tileMap.IsSolid(newHB.xMax + PIXEL, oldHB.yMin))) {
      v.x = 0;
      target.x = Mathf.Floor(newHB.xMax + PIXEL) - PIXEL - hitBoxSize.x / 2;
    } else if (v.x < 0 && (tileMap.IsSolid(newHB.xMin - PIXEL, oldHB.yMax - PIXEL) || tileMap.IsSolid(newHB.xMin - PIXEL, oldHB.yMin))) {
      v.x = 0;
      target.x = Mathf.Ceil(newHB.xMin - PIXEL) + PIXEL + hitBoxSize.x / 2;
    }

    // Then sweep vertical motion
    if (v.y > 0 && (tileMap.IsSolid(oldHB.xMin, newHB.yMax + PIXEL) || tileMap.IsSolid(oldHB.xMax, newHB.yMax + PIXEL))) {
      isGrounded = true;
      target.y = Mathf.Floor(target.y + PIXEL);
      v.y = 0;
    } else if (v.y < 0 && (tileMap.IsSolid(oldHB.xMin, newHB.yMin - PIXEL) || tileMap.IsSolid(oldHB.xMax, newHB.yMin - PIXEL))) {
      isGrounded = false;
      target.y = Mathf.Ceil(target.y - PIXEL);
      v.y = 0;
    } else {
      isGrounded = false;
    }

    origin = target;

    return v;
  }

  public void OnDrawGizmos()
  {
    Collider2D c2d = gameObject.GetComponent<Collider2D>();
    if (c2d) {
      Utility.DrawDebugRect(c2d.bounds.min, c2d.bounds.max);
    }
  }

  public void Kill(bool destroy)
  {
    // Use the player's audio source because this one might get destroyed
    PlayerController player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
    player.audioSource.PlayOneShot(deathSound);
    if (destroy) {
      Destroy(gameObject);
    }
  }
}
