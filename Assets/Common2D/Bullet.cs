using UnityEngine;

public class Bullet : CharacterCore
{
	public bool moveLeft;

	void FixedUpdate() {
		Vector2 after = Move(new Vector2(moveLeft ? -speed : speed, 0));
		if (after.x == 0) {
			Destroy(this.gameObject);
		}
	}
}
